#!/usr/bin/env python3
# -- coding: utf-8 --

import os
import subprocess
import platform
import socket
from collections import Counter

print('\nOppg 1/2/3/4')
print('\nValgte å formatere oppgaven før vi begynner å søke i den.\n'
      'På den måten får vi søke slik vi ville.')


class oppg:

    def ping_localhost(self):
        # Use if controll for os detection for command instead of error handling
        # since it does not actually throw error on linux only wrong IP. No need to pipe results
        # If > piping and checking result cost efficient
        if platform.system() == "Windows":
            os.system('ping localhost -n 1')
        else:
            os.system('ping localhost -c 1')

        print("\n")

    def ping_range(self):
        devnull = open(os.devnull, 'w')
        i = input("Write the 3 first values in the IP + '.', EX(192.168.0.) : ")
        for ping in range(1, 255):
            address = str(i) + str(ping)
            try:
                # Checking with socket for actual IP, if anything else exception will inform user
                socket.inet_aton(address)
                res = subprocess.call(['ping ', '-n ', '1', '-w', '10', address], stdout=devnull)
                if res == 0:
                    print(address, "OK")
                elif res == 2:
                    print("No response from", address)
                else:
                    print(address, "failed")
            except socket.error:
                print("Not correct IP, try again")

    def usernames_passwords(self):
        # just to format the passwordlist to passwordlist3
        # format =  username:password\n
        # The new format is easier to iterate through and it is easier to look at
        #
        def reformat_username_password():
            fh = open('passwordlist.txt', 'r')
            data = fh.readlines()
            fh.close()
            fo = open('passwordlist3.txt', 'w')
            counter = 0
            for name_passw in data:
                words = name_passw.split(';')
                for name_passw in words:
                    liste = name_passw.split(':')
                    for passwords in liste:  # prints passwords, doesn't do anything now, since its commented
                        counter += 1
                        if counter % 2 == 0:
                            # print(passwords)
                            fo.write(name_passw + '\n')

                fo.close()

        reformat_username_password()  # """comment out to not run"""


        f = open("passwordlist3.txt", "r")
        print('File not found')
        content = f.readlines()
        f.close()

        valid_line = [i for i in content if ('' in i and '' in i)]
        firstline = valid_line[0]

        usernames = set()
        for content in valid_line:
            start = content.index('')
            end = content.find(':')
            name = content[start:end].strip()
            usernames.add(name)
        print('All unique usernames sorted alphabetically in the list :\n',
              sorted(usernames), '\n', 'Unique usernames : ',
              len(usernames))

        passw = set()
        for content in valid_line:
            start = content.find(':') + 1
            end = content.find('\n')
            p = content[start:end].strip()
            passw.add(p)
        print('\n\nAll unique passwords sorted alphabetically in the list :\n',
              sorted(passw), '\n', 'Unique passwords : ',
              len(passw))

        passw = list()
        for content in valid_line:
            start = content.find(':') + 1
            end = content.find('\n')
            p = content[start:end].strip()
            passw.append(p)
        print('\n\n', 'All passwords counted (duplicates included): ',
              len(passw))

        userpasses = list()
        for content in valid_line:
            start = content.index('')
            end = content.find(':')
            name = content[start:end]
            userpasses.append(name)

        print('\n\n', 'Each user has this many passwords :')

        c = Counter(userpasses)
        for key, value in c.most_common():
            print(key, value)


while True:
    """
    This error handling is a mix of LBYL and EAFP because we tried to use strictly EAFP,
    so that if the input were anything else than 1-4 we would raise an exception to say sorry,
    you need to type 1-4. We weren't able to take this properly in, so we added EAFP for ValueErrors
    and LBYL for integer check between 1-4: see if statements. Feedback would be appriciated.
    """
    try:
        inp = int(input("1. Ping Localhost (1)\n2. Ping range of IP/takes a while (2)"
                    "\n3. for task 2/3/long output (3)\n4. for exit (4)\nanswer: "))
        run = oppg()
    except ValueError:
        print('Incorrect. Write an integer between 1 - 4')
    else:
        if inp == 1:
            run.ping_localhost()
        elif inp == 2:
            run.ping_range()
        elif inp == 3:
            run.usernames_passwords()
        elif inp == 4:
            break
        elif inp < 1 or inp > 4:
            print('Incorrect. Write an integer between 1 - 4')


if __name__ == '__main__':
    oppg()
