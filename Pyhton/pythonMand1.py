#!/usr/bin/env python3

import random

print("Oppg. 1")
print("--------------------------")
name = ['Petter Svendsen', 'Alex Ingebritsen', 'Olof Engan', 'Alfred Ettervei', 'Rafael Enrique']
username = ['Petsv', 'Alein', 'Oloen', 'Alfet', 'Rafen']

for nam, uname in zip(name, username):
    print(nam, uname)

print("\nOppg. 2")
print("--------------------------")
#stores the generated pins/passwords
password = []
#stores the final cracked pins
crackedPin = []
#number of keys to be generated, var for easy change
numberOfKeys = 10
#number of digits in keys, var for easy change
numberOfDigits = 3

count = 0

while (count < numberOfKeys):
    #temporary list for storing generated keys until all are generated
    #this is done so that multiple pins can be tested and not only one
    tempList = []
    for i in range(numberOfDigits):
        tempList.append(random.randint(0, 9))

    #Put the generated keys into the password list before itteration re-generates it
    password.insert(count, tempList)
    count = count +1
print("Generated pins as", password)

for pins in password:
   # print(pins)
    count = 0
    tempList = []
    while count < numberOfDigits:
        for i in range(0, 10):
            #Checks weither there is a match in the pin with number in range
            #Will add the matched number into templist until itteration is done
            if i == pins[count]:
                #print("Matched ", i, " with ", pins)
                tempList.append(i)

        count = count +1
    crackedPin.append(tempList)

print("Cracked pins are:", crackedPin)


print("\nOppg. 3")
print("--------------------------")
password_list = {"chris": "helloworld", "john": "passw1", "nelly": "2hell1", "wendy": "1Passw"}
for name, passwords in password_list.items():
    print(name, passwords)
print("-------")
p = password_list.values()
for letter in p:
    print(letter)

count = 0
maxIt = len(letter)
tempList = []
for char in letter:
    while count < maxIt:
        if char.isalpha():
            tempList.append(char)
            break
        elif char.isdigit():
            tempList.append(char)
            break
print('Passwords cracked as', ''.join(tempList))
