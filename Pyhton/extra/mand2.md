1) Create a class PingIP that has 2 functions. 
    - One function should ping localhost when you create a object of PingIP
    - Hint: use "ping -c 1" for only send one ping, on windows "ping -n 1"
    - One function should ping the hole range in an ip groupe.(Just like the bash script you have made)
2) The passwordlist.txt file contains usernames and passwords. Create a script to read the file and print:
    - All unique usernames
    - All unique passwords
    - How many passwords there are in total(include duplicates)
    - Count how many different password each user has
3) Create a script to format the passwordlist.txt into a new file with better format, and comment on why the new format is better.
    - Format could be one column with user names and one for passwords.
4) Take the code from task 1-3 and include error handling, use LBYL or EAFP as you see fit, and comment on it.