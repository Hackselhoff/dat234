Here are the task's you need to do:

1.
Create 2 lists, one with usernames and one with names.
Print the output of both lists in this format(dots should not be printed):
Christoffer Berglund: chrisb
Thomas Olsen: thomao

2.
Create a random code of 3 digits. Then crack it. (Brute force)
Hint:

    password = []

    for i in range(3):

        password.append(random.randint(0, 9))

3.
You have obtained a dictionary of usernames and passwords.
You know the password you want is 6 character long,
has one capital letter in it and begins with a number.
Loop the dictionary and print out the username and password.
Use the dictionary under, but think of it as a small sample of a greater list.

- password_list = {"chris":"helloworld", "john":"passw1", "nelly":"2hell1", "wendy":"1Passw"}
- Hint: check if character is a digit: variable.isdigit()
- Hint: check if character is uppercase: variable.isupper()