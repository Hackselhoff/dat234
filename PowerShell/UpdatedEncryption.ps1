﻿#MÅ KJØRES GJENNOM EN GANG FØR DET FUNKER SOM DET SKAL
# DEL 5
#Function for basic standard string encryption of AES. Uses ConvertFrom-SecureString from PS library:> 
# Better than the basic shifting of characters due to implemented encryption technology that needs much more effort to decrypt by force
function encryptSecure ($Secure) {

$Encrypted = ConvertFrom-SecureString -SecureString $Secure

return $Encrypted
}

#Function for decrypting an encrypted string. Uses ConvertTo-SecureString from PS library to decrypt
#Also uses a system call to convert it back into readable unprotected System.String format to print out and
#verify what was typed is decrypted again.
function decryptSecure ($Encrypted) {
$Secure2 = ConvertTo-SecureString -String $Encrypted

$result = [System.Runtime.InteropServices.marshal]::PtrToStringAuto([System.Runtime.InteropServices.marshal]::SecureStringToBSTR($Secure2))
return $result
}

$loop = $true

do{

$Prompt = Read-host "Type 'e' encrypting : 'd' decrypting 'q' exit (e|d|q) : "
Switch($prompt){

e{
echo "Enter the string you wish to be securely encrypted"
$Secure = Read-Host -AsSecureString
echo "String encrypted as"
echo "##### Start of encrypted string #####"
echo $secString
echo "##### End of encrypted string #####"
$secString = encryptSecure($Secure);
echo ""
}

d{
   echo "Decrypting the string back to readable format"
$decString = decryptSecure($secString);
echo "Here is your decrypted string"
echo $decString
}

default{continue}

q{$loop = $false}

  } 
} while($loop)



